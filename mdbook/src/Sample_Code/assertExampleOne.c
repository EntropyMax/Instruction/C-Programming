//////////////ASSERT EXAMPLE 1///////////////////

#include <assert.h>     //defines assert()

#include <stdlib.h>     //defines calloc()
#include <stdio.h>      //defines string-related functions

int main(void)
{
    //string pointer
    char * dynamicString = NULL;
    //dynamic string
    dynamicString = calloc(20, sizeof(char));

    //assert that dynamicString is not NULL
    assert(dynamicString);
    
    //read a string
    fgets(dynamicString, 20, stdin);

   //assert that the string is nul terminated
   assert(dynamicString[19] == '\0');

   //print the string
   puts(dynamicString);
   return 0; 


//////////////ASSERT EXAMPLE 2///////////////////

#include <assert.h>     //defines assert()
#include <stdio.h>      //defines string-related functions

int main(void)
{
    int numerator, denominator;
    float result;
    puts("Enter two numbers to divide (ex. 5 / 3)");
    scanf("%d / %d", &numerator, &denominator);

    //assert that the denominator is not 0
    assert(denominator);

    result = (float)numerator / denominator;
    //print the string
    printf("Result: \t%.2f", result);
    return 0;
}