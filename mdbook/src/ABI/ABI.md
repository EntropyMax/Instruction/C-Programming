
## What is an Application Binary Interface ?

an application binary interface (ABI) is an interface between two binary program modules; often, one of these modules is a library or operating system facility, and the other is a program that is being run by a user.

An ABI defines how data structures or computational routines are accessed in machine code, which is a low-level, hardware-dependent format; in contrast, an API defines this access in source code, which is a relatively high-level, hardware-independent, often human-readable format. A common aspect of an ABI is the calling convention, which determines how data is provided as input to or read as output from computational routines; examples are the x86 calling conventions."


## Calling conventions describe the interface of called code:

* The order in which atomic (scalar) parameters, or individual parts of a complex parameter, are allocated
* How parameters are passed (pushed on the stack, placed in registers, or a mix of both)
* Which registers the called function must preserve for the caller (also known as: callee-saved registers or non-volatile registers)
* How the task of preparing the stack for, and restoring after, a function call is divided between the caller and the callee

This is intimately related with the assignment of sizes and formats to programming-language types. Another closely related topic is name mangling, which determines how symbol names in the code map to symbol names used by the linker. Calling conventions, type representations, and name mangling are all part of what is known as an application binary interface (ABI).

The cdecl (which stands for C declaration) is a calling convention that originates from Microsoft's compiler for the C programming language and is used by many C compilers for the x86 architecture.

syscall
This is similar to cdecl in that arguments are pushed right-to-left. EAX, ECX, and EDX are not preserved. The size of the parameter list in doublewords is passed in AL.

Syscall is the standard calling convention for 32 bit OS/2 API.

The cdecl calling convention is usually the default calling convention for x86 C compilers, although many compilers provide options to automatically change the calling conventions used. To manually define a function to be cdecl, some support the following syntax:
```c
return_type __cdecl func_name();

```

## STDCALL
STDCALL, also known as "WINAPI" (and a few other names, depending on where you are reading it) is used almost exclusively by Microsoft as the standard calling convention for the Win32 API. Since STDCALL is strictly defined by Microsoft, all compilers that implement it do it the same way.

STDCALL passes arguments right-to-left, and returns the value in eax. (The Microsoft documentation erroneously claimed that arguments are passed left-to-right, but this is not the case.)
The called function cleans the stack, unlike CDECL. This means that STDCALL doesn't allow variable-length argument lists.
Consider the following C function:
```c
_stdcall int MyFunction2(int a, int b)
{
   return a + b;
}
```
and the calling instruction:
```c
 x = MyFunction2(2, 3);
 ```
These will produce the following respective assembly code fragments:
```c
:_MyFunction2@8
push ebp
mov ebp, esp
mov eax, [ebp + 8]
mov edx, [ebp + 12]
add eax, edx
pop ebp
ret 8
and

push 3
push 2
call _MyFunction2@8
```
There are a few important points to note here:

In the function body, the ret instruction has an (optional) argument that indicates how many bytes to pop off the stack when the function returns.
STDCALL functions are name-decorated with a leading underscore, followed by an @, and then the number (in bytes) of arguments passed on the stack. This number will always be a multiple of 4, on a 32-bit aligned machine.













