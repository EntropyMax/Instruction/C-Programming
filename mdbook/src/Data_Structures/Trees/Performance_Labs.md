
### The slides for this topic are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Data_Structures/Trees/slides/#/).

# Performance Tree labs

1.)If btree.in contains C E F @ H @ @ B @ @ G A @ @ N J @ @ K @ @, then the program builds the following tree: 

![](../../assets/trees4.png)


2.) Find the root of each of the following binary trees:

a. tree with postorder traversal: FCBDG
b. tree with preorder traversal: IBCDFEN 
c. tree with inorder traversal: CBIDFGE

3.) A binary tree has 10 nodes. The preorder and inorder traversals of the tree are shown below. Draw the tree.

Preorder: JCBADEFIGH
Inorder: ABCEDFJGIH



3.) Draw the binary tree whose in-order and post-order traversals of the nodes are as follows:          
```
In-order:   G D P K E N F A T L 
Post-order:   G P D K F N T A L E
```


4.)Draw the binary tree whose in-order and pre-order traversals of the nodes are as follows:         
```
 In-order:   G D P K E N F A T L 

 Pre-order:   N D G K P E T F A L
```

5.) Fill in the Traversal functions for this C Tree program
```c
// Tree traversal in C

#include <stdio.h>
#include <stdlib.h>

struct node {
  int item;
  struct node* left;
  struct node* right;
};

// Inorder traversal
void inorderTraversal(struct node* root) {
  # Your code here
}

// preorderTraversal traversal
void preorderTraversal(struct node* root) {
  # Your code here
}

// postorderTraversal traversal
void postorderTraversal(struct node* root) {
  # Your code here
}

// Create a new Node
struct node* createNode(value) {
  struct node* newNode = malloc(sizeof(struct node));
  newNode->item = value;
  newNode->left = NULL;
  newNode->right = NULL;

  return newNode;
}

// Insert on the left of the node
struct node* insertLeft(struct node* root, int value) {
  root->left = createNode(value);
  return root->left;
}

// Insert on the right of the node
struct node* insertRight(struct node* root, int value) {
  root->right = createNode(value);
  return root->right;
}

int main() {
  struct node* root = createNode(1);
  insertLeft(root, 12);
  insertRight(root, 9);

  insertLeft(root->left, 5);
  insertRight(root->left, 6);

  printf("Inorder traversal \n");
  inorderTraversal(root);

  printf("\nPreorder traversal \n");
  preorderTraversal(root);

  printf("\nPostorder traversal \n");
  postorderTraversal(root);
}


```



6.) Create the balanced Tree for this Inorder traversal of a tree: 13 17 19 25 27 35 55




7. )Write a program to read the information in storms2.txt. Use the preceding structure and print the hurricane names in alphabetical order: 
```
Strongest Hurricanes between 1950 and 2002 
Hurricane Name 
```


