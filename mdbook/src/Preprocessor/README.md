# The Preprocessor
### Slides for this topic are available [here](./slides)
## items covered:

Coding Format

Stub Code

Definitions

\#include

\#define

\#operator

\#\#

\#undef

Conditional Compilation
