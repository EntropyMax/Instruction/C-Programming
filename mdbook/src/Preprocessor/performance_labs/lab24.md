# PREPROCESSOR DIRECTIVES

### Make a program using preprocessor directives that:
  1. Has a BUFFER_SIZE of 64
  2. Has a macro VICTORY that prints a victory message then exits the program

### Then, inside of main:
  1. Declare a string with a size of BUFFER_SIZE
  2. Take user input as a string
  3. If the user's string is "Tacos", then use the VICTORY macro
