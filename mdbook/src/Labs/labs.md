# Labs



## Performance Labs
---

### [Intro - Documentation](Performance/documentation.md)
    Utilizing MSDN and Man Pages to find proper use of C library funcions

    KSATs: S0154, S0155

### [Arrays - Intro Labs](Performance/Arrays_Perf_labs.md)
    Array terminology, declaring and initalizing arrays, accessing and modifying array elements.

    KSATs: K0104, S0034, S0035

### [Strings - Intro Labs](Performance/Strings_Perf_labs.md)
    General string practice - 12 Labs

    KSATs: S0034, S0036, K0128

### [getchar and putchar](Performance/putchar_getchar_perf.md)
    Practice with getchar and putchar

    KSATs:K0187, S0016

### [getc and putc](Performance/putc_getc_perf.md)
    Practice with getc and putc

    KSATs:K0187, S0016

### [String I/O Functions](Performance/string-io-perf.md)
    Practice with fgets, puts and fputs

    KSATs: K0188, K0187, S0016

### [Scanf Lab](Performance/scanf_perf.md)
    Using scanf to handle user input

    KSATs: K0188, K0187, S0016

### [Assignment, Relational, Logical Operators](Performance/Operators_perf.md)
    Practice using the different types of operators

    KSATs: S0108, S0030, S0031, S0032, S0033, K0714, K0715, K0735, K0764, S0016

### [Operators and Order of Operations Labs](Performance/OrderOfOperations_perf.md)
    Several practice problems involving operators and order of operations

    KSATs: K0716, S0029, S0016

### [Bitwise Lab 1](../Bitwise_operators/performance_labs/Perf_labs.md)
    Bitshift inputted int Left and Right

    KSATs: S0030, K0111, S0016

### [Bitwise Lab 2](../Bitwise_operators/performance_labs/lab10.md)
    Bitwise operators practice

    KSATs: S0030, K0111, S0016

### [Bitwise Lab 3](../Bitwise_operators/capstone-1.md)
    Flip a single bit using XOR

    KSATs: S0030, K0111, S0016

### [If Statements](Performance/ifs_perf.md)
    Input and if statements

    KSATs: K0754, S0082, S0016

### [If - Else 1](Performance/if_else_1.md)
    Using If/Else to determine if a number is even or odd

    KSATs: K0754, S0082, S0016

### [If - Else 2](Performance/if_else_2.md)
    Using If statements and bitwise to ensure that a number is positive

    KSATs: K0754, S0082, S0016

### [Else if 1](Performance/if_else_1.md)
    Using an else if to conditionally perform math operations

    KSATs: K0754, S0082, S0016

### [Else if 2](Performance/else_if_2.md)
    Additional Else If practice
    
    KSATs: K0754, S0082, S0016

### [Switch](Performance/switch_1.md)
    Making a menu using a switch statement

    KSATs: S0083, K0755, S0016

### [Switch 2](Performance/switch_2.md)
    Using a switch statement to evaluate a simple math formula

    KSATs: S0083, K0755, S0016

### [For Loops](Performance/for_loops.md)
    Using for loops to improve a previous lab

    KSATs: S0081, K0107, S0016

### [While Loops](Performance/while.md)
    Using while loops to print a string until the null terminator

    KSATs: S0081, K0107, S0016

### [Do While Loops](Performance/while.md)
    Using a do while loop to input a string

    KSATs: S0081, K0107, S0016

### [Break & Continue](Performance/break.md)
    Using continue to check for division by 0

    KSATs: S0081, K0107, S0016

### [Nesting](Performance/break.md)
    Practice using nested control flow

    KSATs: S0081, K0107, K0754, S0082, S0016

### [Functions - Substitutions](Performance/healthy_substitutions.md)
    Create a function to replace letters in a string

    KSATs: K0739, S0048, S0033, S0036, S0016

### [Recursion](../Functions/performance_labs/Perf_labs.md)
    Multiple practice problems for learning recursion

    KSATs: K0737, S0049, S0016
    
### [Pointers - Intro Lab](Performance/memOpsLab.md)
    Making and using pointers

    KSATs: K0103, S0099, S0097, S0016

### [Pointers - String Splitter](Performance/PerLab_string_splitter.md)
    Use address arithmetic to split a string at a delimiter

    KSATs: K0103, S0099, S0097, S0016

### [Pointers - Surfin Bird](Performance/surfin_bird.md)
    Using address arithmetic to return a pointer to a substring

    KSATs: K0103, S0099, S0097, S0016

### [2D Arrays - Tic Tac Toe](Performance/tic_tac_toe.md)
    Using 2d arrays to create a Tic Tak Toe game

    KSATs: K0104, S0016

### [Performance Lab](Performance/texas.md) 
    KSATs: S0098, S0016

### [File I/O - Mighty Morphin'](Performance/mightyMorphin.md)
    Opening and printing a file using getc.

    KSATs: A0179, S0042, S0043, S0016

### [File I/O - Your Song](Performance/yourSong.md)
    Reading and printing a file using fgets.

    KSATs: A0179, S0042, S0043, S0016

### [File I/O - Copy Content](Performance/copyContent.md)
    Reading and writing to files

    KSATs: A0179, S0042, S0043, S0044, S0016

### [File I/O - Usernames](Performance/usernames.md)
    Writing user input to a file

    KSATs: A0179, S0042, S0043, S0044, S0016

### [Memory Lab - Haystack Needle](Performance.Performance/memLab.md)
    Remove the needle from the haystack AKA: Move the substring from the string using memory operators and functions.

    KSATs: S0090, S0091, S0160, S0016

### [Memory Lab - Valgrind](Performance/Valgrind_perf_labs.md)
    Practice using valgrind to find memory issues

    KSATs: S0090, S0091, A0082, S0160, S0016

### [Struct Lab - Surfin Bird](Performance/structLab.md)
    Using structs as function output

    KSATs: S0156, S0016

### [Assert Lab](Performance/assertLab.md)
    Using assert to verify input.

    KSATs: K0123, S0110, S0016

### [Errno Lab](Performance/errnoLab.md)
    Using Errno

    KSATs: K0123, S0110, S0016

### [Asset & Errorno](Performance/Lab_output.md)
    Using both assert & errno

    KSATs: K0123, S0110, S0016

## Demonstration Labs
---
### [Demonstration Lab - Compiling](Demonstration/Greetings.md)
    Using GCC/MinGW and VSCode to compile

    KSATs:

### [Demonstration Lab - Data Types](Demonstration/DataTypes.md)
    Overview of C Data Types

    KSATs: K0096, S0034

### [Demonstration Lab - sizeOf()](Demonstration/SizeOf.md)
    Use of sizeOf(), Size of C Data types

    KSATs: K0096, S0034

### [Demonstration Lab - Data Types Additional Practice](Demonstration/VariableDemos.md)
    Practice with variables, data types, and I/O.

    KSATs: K0096, S0034

### [Demonstration Lab - If Statements](Demonstration/ifs_demo.md)
    Using if statements

    KSATs: K0754, S0082

### [Demonstration Lab - Break Statement](Demonstration/break_demo.md)
    Using breaks while creating a times table

    KSATs: S0081, K0107

### [Demonstration Lab - Arrays](Demonstration/accessModifyArrays.md)
    Accessing, initializing and modifying arrays

    KSATs: K0104, S0034, S0035

### [Demonstration Lab - Function](Demonstration/function-prototypes.md)
    Making and using a function and function prototype

    KSATs:  K0739, S0048, S0033, S0036, S0016

### [Demonstration Lab - Memory Operators](Demonstration/memoryOps.md)
    Making and derefernecing pointers

    KSATs: K0103, S0099, S0097, S0016

### [Demonstration Lab - Smallest Int](Demonstration/LittleInt.md)
    Using Address Arithmetic to find the smallest int in an array

    KSATs: K0103, S0099, S0097, S0016

### [Demonstration Lab - Battleship](Demonstration/battleship)
    Using 2D arrays to create a battleship game

    KSATs: K0104, S0016

### [Demonstration Lab - getchar and putchar](Demonstration/getchar_putchar.md)
    Using the getchar and putchar functions

    KSATs: K0188, K0187, S0016

### [Demonstration Lab - getc and putc](Demonstration/getc_putc.md)
    Using the getc and putc functions

    KSATs: K0188, K0187, S0016

### [Demonstration Lab - Other IO Functions](Performance/string-io-perf.md)
    Using fgets, puts and fputs

    KSATs: K0188, K0187, S0016

### [Demonstration Lab - Safe Input with Scanf](Demonstration/scanf_1.md)
    Using scanf to safely read strings

    KSATs: K0188, K0187, S0016

### [Demonstration Lab - Formatted Scanf](Demonstration/scanf_2.md)
    Using formatted scanf to handle int input.

    KSATs: K0188, K0187, S0016
    
### [Demonstration Lab - Function Pointers](Demonstration/cliCalculator.md)
    Using Function pointers to make a cli calculator

    KSATs: S0098, S0016

### [Demonstration Lab - Reading Files](Demonstration/readingDemo1.md)
    File IO reading one line or one character at a time using getc and fgets.

    KSATs: A0179, S0042, S0043, S0016

### [Demonstration Lab - Memory Management](Demonstration/MemDemo1.md)
    Malloc, strcopy, strcmp, memcopy, memmove, strstr

    KSATs: S0090, S0091, A0082, S0160, S0016

### [Demonstration Lab - Malloc 1](Demonstration/mallocDemo1.md)
    Malloc and free walkthrough

    KSATs: S0090, S0091, A0082, S0160, S0016

### [Demonstration Lab - Malloc 2](Demonstration/mallocDemo2.md)
    Malloc an array of ints

    KSATs: S0090, S0091, A0082, S0160, S0016

### [Demonstration Lab - Valgrind](Demonstration/valgrind_demo_labs.md)
    Use valgrind's memcheck to check for issues with memory

    KSATs: S0090, S0091, A0082, S0160, S0016

### [Demonstration Lab - Structs](Demonstration/structDemo.md)
    Demonstrating the use of Structs

    KSATs: S0156, S0016

### [Demonstration Lab - Make a LL](Demonstration/LLDemo1.md)
    Defining and creating a linked list

    KSATs: S0055, A0735, S0016

### [Demonstration Lab - Print a LL](Demonstration/LLDemo2.md)
    Printing every element in a linked list

    KSATs: S0055, A0735, S0016

### [Demonstration Lab - Assert](Demonstration/assertDemo.md)
    Demonstrating the use of Assert

    KSATs: K0123, S0110, S0016

### [Demonstration Lab - Errno](Demonstration/Errno.md)
    Demonstrating the use of Errno

    KSATs: K0123, S0110, S0016