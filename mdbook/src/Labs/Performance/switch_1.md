## Student Practice lab: MAY I SEE A MENU?

* Input an integer from the user
* Create a menu allowing the user to:
    * Print their input as the following...
        * Binary
        * Octal
        * Decimal
        * Hexadecimal
    * Input a new character
    * Input a new number
    * exit
