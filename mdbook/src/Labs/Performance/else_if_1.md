## Student Practice lab: WHATS THE DIFFERENCE?

* Initialize ONLY three **int** variables to 0.
* Safely scan user input into variables #1 and #2 utilizing a single line.
* Using a single ELSE-IF statement:
    * If variable #1 is equal to variable #2, assign 0 to variable #3.
    * Otherwise, subtract the smallest from the largest (i.e. 3-2, 42-(-45), (-11)-(-1337), etc...) and assign the result to variable #3.
* Print the value of variable #3 if it is positive otherwise print an ERROR.
