# Demonstration Lab

## Basic I/O b

* Input a single character then write that character to stdout

```c
int userInput = 0;                // Will store input
printf("Enter a character: ");    // Prompts user
userInput = getc(stdin);          // Stores stream input
printf("Your character was : ");  // Prefaces output
putc(userInput, stdout);          // Writes to stdout
```

### Discuss the output of...

* Enter a character: **7**
* Enter a character: **m**
* Enter a character: **54**
* Enter a character: **JasV**
