# You Sunk My Battleship!

![](../Pointers_Arrays/assets/Batt1.jpg)

### Part I:
- Use two two-dimensional arrays to replicate a single player’s game piece from Hasbro’s board game “Battleship”
- Initialize one of the arrays with your ship placement
- Print both arrays to stdout in a human-readable format

### Part II:
- Define pass-by-reference functions to replicate some aspect of the Hasbro game of Battleship
- print_grid()
- place_ships()
- verify_empty()
- FIRE()
- under_fire()
