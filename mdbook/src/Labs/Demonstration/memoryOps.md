# Demonstration Lab - Memory Operators

**Note to Instructor:** ‘Live code’ this for the class on the big screen. Let the class decide on appropriate uses/implementations of each of the requirements.

- Declare two variables, named var1 and var2, of the same data type
- Declare a pointer variable, named var_ptr, of the same data type
- Define the first variable with an arbitrary value
- Assign var1’s memory address to var_ptr
- Define var2 by dereferencing var_ptr
- Compare var1 to var2 and print human-readable results
