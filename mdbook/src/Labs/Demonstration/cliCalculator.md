# Command Line Calculator

```c
double add(double firstNumber, double secondNumber);
double subtract(double firstNumber, double secondNumber);
double multiply(double firstNumber, double secondNumber);
double divide(double firstNumber, double secondNumber);
```

- Define the function parameters above
- Use formatted input (e.g., scanf) to take input which will obviously invoke one of the functions above (e.g., 5.1 + 7.2, -1.9 - 1.3, -13.23 * 13.37, 1 / 2.3)
- Use a conditional statement to choose the right function based on the mathematical operator and assign it to a function pointer
- Use that function pointer perform the calculation
- Print the results in a human readable format
- Beware NULL pointers and “divide by 0” errors
