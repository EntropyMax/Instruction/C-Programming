## Demonstration Lab 3

## String I/O

* Read a string from stdin and then write that string to stdout:

```c
char buff[4];                        // Will store string
printf("Enter string: ");            // Prompts user
fgets(buff, sizeof(buff), stdin);    // Stores user string
printf("Your string was: ");         // Prefaces output
puts(buff);                          // Writes to stdout
```

### Discuss the output of...

* Enter a string: **123**
* Enter a string: **abcd**
* Enter a string: **31398**
* Enter a string: **Superman**