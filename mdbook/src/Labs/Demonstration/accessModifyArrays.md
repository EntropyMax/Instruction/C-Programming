## Demonstration Lab 1

#### Tasks

* **Declare and initialize the following arrays:**

| **data type** | **name** | **dimension** | **initialize to:** |
| :--- | :--- | :--- | :--- |
| int | myIntArray | 10 | 100 (every index) |
| float | myFloatArray | 5 | 1-5 |
| char | myCharArray | 256 | 0 (every index) |

* #### **print the 3rd element of each array above using:**

| **array data type** | **syntax** |
| :--- | :--- |
| int | printf("%d\n", myIntArray[2]); |
| float | printf("%f\n", myFloatArray[2]); |
| char | printf("%c\n", myCharArray[2]); |

* **Perform the following manipulations on your arrays:**

  * **myIntArray** - set all elements to x while:
    * y = index \#
    * x = \(y+1\) \* 10
  * **myFloatArray** - set all elements to x while:
    * y = current value of a given index
    * x = y \* 1.1
  * **myCharArray** - fill in the beginning elements with your last name starting with index 0
* **Print the third index of each array again**

---