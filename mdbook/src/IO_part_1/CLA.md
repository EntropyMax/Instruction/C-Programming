# Command Line Arguments in C
The arguments passed from command line are called command line arguments. These arguments are handled by main() function.

To support command line argument, you need to change the structure of main() function as given below.
```c
int main(int argc, char *argv[] )  
```
Here, argc counts the number of arguments. It counts the file name as the first argument.

The argv[] contains the total number of arguments. The first argument is the file name always.

Example
Let's see the example of command line arguments where we are passing one argument with file name.
```c
#include <stdio.h>  
void main(int argc, char *argv[] )  {  
  
   printf("Program name is: %s\n", argv[0]);  
   
   if(argc < 2){  
      printf("No argument passed through command line.\n");  
   }  
   else{  
      printf("First argument is: %s\n", argv[1]);  
   }  
}  
```
Run this program as follows in Linux:
```
./program hello 
``` 
Run this program as follows in Windows from command line:
```
program.exe hello  
```
Output:
```
Program name is: program
First argument is: hello
```
If you pass many arguments, it will print only one.
```
./program hello c how r u  
```
Output:
```
Program name is: program
First argument is: hello
```
But if you pass many arguments within double quote, all arguments will be treated as a single argument only.
```
./program "hello c how r u"  
```
Output:
```
Program name is: program
First argument is: hello c how r u
```
You can write your program to print all the arguments. In this program, we are printing only argv[1], that is why it is printing only one argument.

## Properties of Command Line Arguments:
```
They are passed to main() function.
They are parameters/arguments supplied to the program when it is invoked.
They are used to control program from outside instead of hard coding those values inside the code.
argv[argc] is a NULL pointer.
argv[0] holds the name of the program.
argv[1] points to the first command line argument and argv[n] points last argument.
```

Note : You pass all the command line arguments separated by a space, but if argument itself has a space then you can pass such arguments by putting them inside double quotes “” or single quotes ”.
```c
// C program to illustrate 
// command line arguments 
#include<stdio.h> 
  
int main(int argc,char* argv[]) 
{ 
    int counter; 
    printf("Program Name Is: %s",argv[0]); 
    if(argc==1) 
        printf("\nNo Extra Command Line Argument Passed Other Than Program Name"); 
    if(argc>=2) 
    { 
        printf("\nNumber Of Arguments Passed: %d",argc); 
        printf("\n----Following Are The Command Line Arguments Passed----"); 
        for(counter=0;counter<argc;counter++) 
            printf("\nargv[%d]: %s",counter,argv[counter]); 
    } 
    return 0; 
} 
```
## Output in different scenarios:

Without argument: When the above code is compiled and executed without passing any argument, it produces following output.
```
$ ./a.out
Program Name Is: ./a.out
```
No Extra Command Line Argument Passed Other Than Program Name
Three arguments : When the above code is compiled and executed with a three arguments, it produces the following output.
```
$ ./a.out First Second Third
Program Name Is: ./a.out
Number Of Arguments Passed: 4
----Following Are The Command Line Arguments Passed----
argv[0]: ./a.out
argv[1]: First
argv[2]: Second
argv[3]: Third
```
Single Argument : When the above code is compiled and executed with a single argument separated by space but inside double quotes, it produces the following output.
```
$ ./a.out "First Second Third"
Program Name Is: ./a.out
Number Of Arguments Passed: 2
----Following Are The Command Line Arguments Passed----
argv[0]: ./a.out
argv[1]: First Second Third
```
Single argument in quotes separated by space : When the above code is compiled and executed with a single argument separated by space but inside single quotes, it produces the following output.
```
$ ./a.out 'First Second Third'
Program Name Is: ./a.out
Number Of Arguments Passed: 2
----Following Are The Command Line Arguments Passed----
argv[0]: ./a.out
argv[1]: First Second Third
```







