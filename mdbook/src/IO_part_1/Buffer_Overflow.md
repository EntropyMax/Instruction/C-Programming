# Buffer Overflow

A buffer is a temporary area for data storage. When more data (than was originally allocated to be stored) gets placed by a program or system process, the extra data overflows. It causes some of that data to leak out into other buffers, which can corrupt or overwrite whatever data they were holding.

## Buffer Overflow Attacks
Until now we discussed about what buffer overflows can do to your programs. We learned how a program could crash or give unexpected results due to buffer overflows. Horrifying isn’t it ? But, that it is not the worst part.

It gets worse when an attacker comes to know about a buffer over flow in your program and he/she exploits it. Confused? Consider this example (Adapted from [this article](https://www.thegeekstuff.com/2013/06/buffer-overflow/)) :
```c
#include <stdio.h>
#include <string.h>

int main(void)
{
    char password[15];
    int passed = 0;

    printf("Password:\n");
    gets(password);

    if(strcmp(password, "90cos"))
    {
        printf ("Wrong Password!\n");
    }
    else
    {
        printf ("Correct Password!\n");
        passed = 1;
    }

    if(passed)
    {
       /* Now Give root or admin rights to user*/
        printf ("Access Granted!");
    }

    return 0;
}
```
The program above simulates scenario where a program expects a password from user and if the password is correct then it grants root privileges to the user.

Let’s the run the program with correct password ie ‘90cos’ :
```c
$ gcc buffer.c -o bfrovrflw -fno-stack-protector
$ ./bfrovrflw 

Password :
90cos

Correct Password!

Access Granted!
 ```
This works as expected. The passwords match and root privileges are given.

But do you know that there is a possibility of buffer overflow in this program. The gets() function does not check the array bounds and can even write string of length greater than the size of the buffer to which the string is written. Now, can you even imagine what can an attacker do with this kind of a loophole?

Here is an example :
```c
$ ./bfrovrflw 
Password:
hhhhhhhhhhhhhhhhhhhh

Wrong Password! 

Access Granted!
 ```
In the above example, even after entering a wrong password, the program worked as if you gave the correct password.

There is a logic behind the output above. What attacker did was, he/she supplied an input of length greater than what buffer can hold and at a particular length of input the buffer overflow so took place that it overwrote the memory of integer ‘pass’. So despite of a wrong password, the value of ‘pass’ became non zero and hence root privileges were granted to an attacker.

There are several other advanced techniques (like code injection and execution) through which buffer over flow attacks can be done but it is always important to first know about the basics of buffer, it’s overflow and why it is harmful.

To avoid buffer overflow attacks, the general advice that is given to programmers is to follow good programming practices. For example:

Make sure that the memory auditing is done properly in the program using utilities like "valgrind" memcheck
Use fgets() instead of gets().
Use strncmp() instead of strcmp(), strncpy() instead of strcpy() and so on.

## How are buffer overflow errors are made?

These kinds of errors are very easy to make. For years they were a programmer’s nightmare. The problem lies in native C functions, which don’t care about doing appropriate buffer length checks. Below is the list of such functions and, if they exist, their safe equivalents:

- gets() -> fgets() - read characters
- strcpy() -> strncpy() - copy content of the buffer
- strcat() -> strncat() - buffer concatenation
- sprintf() -> snprintf() - fill buffer with data of different types
- (f)scanf() - read from STDIN
- getwd() - return working directory
- realpath() - return absolute (full) path

Use safe equivalent functions, which check the buffers length, whenever it’s possible. Namely:

- gets() -> fgets()
- strcpy() -> strncpy()
- strcat() -> strncat()
- sprintf() -> snprintf()

Functions that don’t have safe equivalents should be rewritten with safe checks implemented. Time spent on that will benefit in the future. Remember that you have to do it only once.

Use compilers, which are able to identify unsafe functions, logic errors and check if the memory is overwritten when and where it shouldn’t be.

