# ELSE-IF statements

An if statement may also include multiple else conditions.

else-if  statements tests an if expression as TRUE or FALSE.

When the expression is TRUE, the if code block will be executed.

When the expression is FALSE, the first else-if expression will be tested in turn.

Else-if expressions are tested for TRUE in turn.

The else code block will be executed when the if and all else-if conditions are evaluated to false.

An if statement may also check multiple conditions.

```c
///else-if statement syntax///

if (expression1)
{
    statement1;
}
else if (expression2)
{
    statement2;
}
else
{
    statement3;
}

```

ELSE-IF example:

```c
///else-if statement examples///

if (i > u)
{
    printf("I is greater than u. \n");
}
else if (i < u)
{
    printf("u is greater than i. \n");
}
else
{
    printf("u and i are equal. \n");
}

```

NOTE: Do not leave else-if statements unwrapped {}!


