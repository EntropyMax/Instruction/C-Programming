# IF-ELSE statements

An if statement may also include an else, these conditional statements test an expression as TRUE or FALSE.

When the expression is TRUE the if code block will be executed...otherwise, the else code block is executed instead.

```c
///if-else statement syntax///

if (expression) //expression is evaluated for true or false
{
    statement1;     //executed when expression is true
}
else
{
    statement2;     //executed when expression is false
}
/* The block containing statement1 is executed when expression is true (expression != 0). Same goes for to the block containing statement2 where the expression is false (expression == 0)*/

///if-else statement examples///

if (i >= u)			//example 1 start
{
    printf("u is no greater than i. \n");
}
else
{
    printf("u is greater than i. \n");
}					//example 1 end

if (i && i == u)	//example 2 start
{
    printf("apparently, this works and i evaluates to true. \n");
}

else
{
    i = u			//example 2 end
}

/* set i equal to u since it's either 0 or not already equivalent */
```

NOTE: Do not leave if-else statements unwrapped {}.

