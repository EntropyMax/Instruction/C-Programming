# DO WHILE Loops
Do while loops utilize a control structure with one of the components written as an expression.

The "loop body" is executed prior to evaluating the control structure.

The component in the head of the do while loop is evaluated:

    if TRUE, the loop body is executed and the expression is evaluated again
    if FALSE, the program continues with the statement following the loop body

***The EXPRESSION is a requirement***

#### Note: take great care to avoid INFINITE LOOOOOOOOOOOOOPS!

```c
///do while loop syntax///
// The loop body will always execute at least once
do				// Execute the following “loop body”…
{				
	statement1;		 // Regular code
	iteration-statement;	// Ensure you control expression1
}
while (expression1);		// …then evaluate expression1
/* Terminating conditions must be included in the loop body */
/*
Sequence of for loop execution:
	1.  Execute the code block
	2.  Is expression1 true?
		2.a.  If so,
			2.a.i   execute the code block again
			2.a.iii go back to 2.
		2.b.  If not, stop looping 
*/

///do while loop example///
char alpha[] = {“Shadow Warrior”};
int i = 0;          // Counting variable

do		
// Execute this…
}
// Print one array element
	printf(“%c\n”, alpha[i]);
// Increment the counter
	i++;
// Is alpha[i] != 0?
}
//stops at null
while(alpha[i]);
                
// Print the number of iterations
printf(“%d characters.\n”, i);

///do while loop example output///
S
h
a
d
o
w

W
a
r
r
i
o
r
14 characters.
```

## When do I use FOR loops?

When there is a known number of iterations.
```c
/*how many maximum iterations do i need to traverse this string?*/
char myString[31] = {"try hardened slugs each movie"};
int i = 0;						//incrementing variable

for (i = 0; i < 31; i += 4)		//what are the values of i?
{
	printf("%c", myString[i]);	//prints one array element
}
putchar(10);					//prints a newline
/* BONUS: what is the output of this FOR loop?*/
```

### When do I use a WHILE loop?

When there are unknown number of iterations ranging from 0-1++.
```c
/*function to print a null-terminated string */
int print_string(char* string)
{
	int i = 0;				//incrementing variable
	if (string)				//checks for NULL pointer
	{
		while (string[i])	//stops on NULL character
		{
			putchar(string[i++]);	prints one element
		}
	}
}
```
### When do I use a DO WHILE loop?

When there is an unknown number of iterations ranging from 1 to 1++.
```c
int inputNumber = 0;					//temporarily holds user input
int runningTotal = 0;					//holds the total
/*continues adding the user input to running sum until 0 s entered*/
do
{									//loop body runs at least once
	printf("enter an interger to add to the total \n");
	printf("(0 to exit) \n");		//this is the exit condition
	scanf("%d", &inputNumber);		//takes user input
	runningTotal += inputNumber;	//adds user input to total
}
while (inputNumber);				//do again if inputNumber != 0
printf("the total is: %d \n", runningTotal);
```

### Why should I use FOR instead of WHILE or vice-versa?

There isn't a definitive guideline when to use a particular type of loop over another.

common convention:

	-use a FOR when the compiler knows how many iterations to run
	-use a WHILE or DO WHILE when the compiler doesn't know

## COMPLETE PERFORMANCE LAB 17