# Variables in C


### To see the slides for this lesson, click [here](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Variables/slides)

## This Topic covers:

- Coding Style Guide
- Variable Names
- Data Types
- Sizes
- Declarations
- Initialization
- Keyword
- Type Conversions

## By the end of this lesson you should know:

- How to utilize variables
  - declaring types
  - setting sizes 
  - initializing them
  - etc
